# Project

With this docker repository, we e have crated a starting point to start fast. 
This docker exists of a recent Laravel installation configured with:
[Laravel Breeze](https://laravel.com/docs/8.x/starter-kits#laravel-breeze)

## Artisan commands and help

### Login into the app container
```bash
docker exec -i -t -w /var/www/todolist.dq.local.dolphiq.eu todo_app bash 
```

After login into the App container, you can run artian or npm commands, some examples:

#### Run css /javascript watcher
```
npm run watch
```

#### Run fresh migration with seeders
```
php artisan migrate:fresh --seed
```

## Create a todo list

For the todolist there is already a starting point created with laravel breeze. You can login into the application using:

### Login into Laravel Breeze
http://todo.dq.local.dolphiq.eu:8080/login
```
user: user@dolphiq.nl
password: secret
```

## Requirements
- If I navigate to http://todo.dq.local.dolphiq.eu:8080 i would like to see a list with the last 10 todo items from all the users in the system.
the list should have the following fields: date, title, created by, Status (done/ open) and username.
- When i login into the system with a valid user, you should see the same list on a dashboard, but only the tasks from that user and
 with some extra buttons: Delete task, Add task and edit task.
- This CRUD should be fully functional. And protected to a user valid user. You should be able to Create, Edit, Delete  a task.
- When I add a new task, it should be bound to the loged-in user.

## Minimal Laravel backend implementations
- Migrations
- Factories
- Seeders
- Blade templates
- Models
- Relations in models
- Routing
- Resource routing
- Request validation
- Middleware to protect some routes

## Minimal Laravel frontend implementations
- Blade tempaltes
- Make use of Tailwind css
- Javascript additions is a can-have 

## Project conditions
- PSR code styling standard
- If required, comments in English.
- Commits should be in English and should be committed to a feature/ branch
- Feature branches should be merged to the development branch using a Pull request.

