# Docker config

## Install docker

## Used versions
- PHP 7.4
- Composer 2
- Node 15

#### Install project(s) into the docker
```bash
git clone -b development git@bitbucket.org:dq-private/dq-todolist-xander.git dq-todolist
cd dq-todolist
chmod +x localbuild/local_install.sh
localbuild/local_install.sh
```
## Application

### Installed Laravel packaages
- Telescope
- Laravel Breeze (Blade/Tailwind)

#### Login into de app container
```bash
docker exec -i -t -w /var/www/todolist.dq.local.dolphiq.eu todo_app bash 
```

#### Build assets with watch (for development)
```bash
docker exec -i -t -w /var/www/todolist.dq.local.dolphiq.eu todo_app bash 
npm run watch
```

#### Build assets with dev build
```bash
docker exec -i -t -w /var/www/todolist.dq.local.dolphiq.eu todo_app bash 
npm run dev
```

#### Database details 
```
host: db
databse: dq_todo
user: db
password: db@123
```

#### Checkout development branch and run migrations ####
```
./localbuild/update_dev.sh
```

### Telescope debug window
http://todo.dq.local.dolphiq.eu:8080/telescope


### Next steps
In the next steps, you will create a todo list!
[More information](TODOLIST.md)