#!/bin/bash

# stop script on error
set -e

function coloredEcho(){
    local exp=$1;
    local color=$2;
    if ! [[ $color =~ '^[0-9]$' ]] ; then
       case $(echo $color | tr '[:upper:]' '[:lower:]') in
        black) color=0 ;;
        red) color=1 ;;
        green) color=2 ;;
        yellow) color=3 ;;
        blue) color=4 ;;
        magenta) color=5 ;;
        cyan) color=6 ;;
        white|*) color=7 ;; # white or invalid color
       esac
    fi
    tput setaf $color;
    echo $exp;
    tput sgr0;
}

SCRIPT_PATH="$(
  cd "$(dirname "$0")"
  pwd -P
)"


coloredEcho "Local installation path: "`pwd` magenta


chmod +rwx localbuild/repos

# Check if you have access to the required repo's
while IFS="" read -r line || [ -n "$line" ]; do
    REPO_LIST+=("${line[0]}")
done < ${SCRIPT_PATH}/repos

for repo in "${REPO_LIST[@]}"; do
  echo "check access to: $repo"

  if git ls-remote git@bitbucket.org:dq-private/$repo >/dev/null; then
    coloredEcho "Yes, access to: $repo" green
  else
    coloredEcho "NO access to $repo" red
    exit 1
  fi
done


echo "Stop all running docker containers"
docker stop $(docker ps -a -q) 2>/dev/null

echo "Prune all closed containers"
docker container prune -f

echo "Remove unused vlan networks"
docker network prune -f

echo "Build the composer containers"
docker-compose build --parallel --force-rm --no-cache

echo "Start app and db container, to set-up the project"
docker-compose up -d

# echo "Clone all the repositories"
# git clone -b development git@bitbucket.org:dq-private/iss-portal.git www/todolist.dq.local.dolphiq.eu
cp -p www/todolist.dq.local.dolphiq.eu/.env.example www/todolist.dq.local.dolphiq.eu/.env

# echo "Add git commit hooks"
# cp localbuild/config/git-pre-commit-hook www/todolist.dq.local.dolphiq.eu/.git/hooks/pre-commit
# chmod +x www/todolist.dq.local.dolphiq.eu/.git/hooks/pre-commit

echo "DB - Wait for MySQL container to be ready..."
docker exec todo_db bash /etc/mysql/conf.d/wait.sh

echo "Run composer and artisan commands in the app container"

docker exec -i \
-w /var/www/todolist.dq.local.dolphiq.eu \
todo_app \
/bin/bash \
  << HERE

  composer install
  chmod -R 777 storage bootstrap/cache
  php artisan key:generate
  php artisan view:clear
  php artisan cache:clear
  php artisan route:clear
  php artisan migrate
  php artisan db:seed
  npm install
  npm run dev

HERE

coloredEcho "Portal is ready at: http://todolist.dq.local.dolphiq.eu:8080/" green

