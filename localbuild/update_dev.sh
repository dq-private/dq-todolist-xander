#!/bin/bash

# stop script on error
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_DIR=$DIR"/../"
APP_CONTAINER="todo_app"

echo $PROJECT_DIR
# todo: change the path to the main folder


directories=(
  'todolist.dq.local.dolphiq.eu'
)
for directory in "${directories[@]}"
do
  echo "Checkout: $directory"
  cd $PROJECT_DIR"www/"$directory
  git config pull.rebase false
  if git checkout development ; then
      git pull
  else
     echo "Can not checkout development in path: $directory"
     exit 1
  fi
done

docker exec \
  -i \
  -w /var/www/todolist.dq.local.dolphiq.eu \
  $APP_CONTAINER \
  /bin/bash \
  << HERE
    echo "Install composer packages if required"
    composer install

    echo "Install npm packages"
    npm install
    npm run prod

    php artisan telescope:publish

    echo "Clear views"
    php artisan view:clear
    php artisan cache:clear
    php artisan route:clear

    echo "Run migrate fresh with seeder"
    php artisan migrate:fresh --seed

HERE



